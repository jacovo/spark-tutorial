package org.spark.demo

import java.nio.file.{Files, Paths}
import java.time.Instant

import org.apache.spark.sql.types._

object Runner extends SparkInit {

  def epoch(ts: String): Long = Instant.parse(ts).getEpochSecond

  def intersect(s1: Session, s2: Session, rangeInSeconds: Int): Boolean =
    (Math.abs(s1.start - s2.start) > rangeInSeconds, Math.abs(s1.end - s2.end) > rangeInSeconds, Math.abs(s1.end - s2.start) > rangeInSeconds, Math.abs(s1.start - s2.end) > rangeInSeconds) match {
      case (true, true, true, true) => false
      case _ => true
    }

  def merge(s1: Session, s2: Session): Session = Session(Math.min(s1.start, s2.start), Math.min(s1.end, s2.end), s1.size + s2.size, s1.tracks ::: s2.tracks)

  val aggMerge: (List[Session], Session) => List[Session] = (sessions, session) => sessions match {
    case Nil => List(session)
    case _ => if (intersect(session, sessions.head, 60 * 20)) merge(session, sessions.head) :: sessions.tail else session :: sessions
  }

  val chartSchema = StructType(
    Array(
      StructField("userid", StringType, nullable = false),
      StructField("timestamp", StringType, nullable = false),
      StructField("artid", StringType, nullable = false),
      StructField("artname", StringType, nullable = false),
      StructField("traid", StringType, nullable = false),
      StructField("traname", StringType, nullable = false)
    )
  )

  def main(args: Array[String]): Unit = {
    import spark.implicits._

    args.headOption match {
      case None => println("Error: input file has not been specified.")
      case Some(source) if !Files.isRegularFile(Paths.get(source)) => println(s"Error: the specified input file <$source> is not a valid file.")
      case Some(source) =>
        val rdd = reader.option("delimiter", "\t").schema(chartSchema).csv(source).as[Chart].rdd
        TopChart(rdd, 10, 50).execute()
    }
    close()
  }
}