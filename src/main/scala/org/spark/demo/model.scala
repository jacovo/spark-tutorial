package org.spark.demo

import org.apache.spark.rdd.RDD

case class Chart(userid: String, timestamp: String, artid: String, artname: String, traid: String, traname: String)

case class Session(start: Long, end: Long, size: Int, tracks: List[String]) extends Ordered[Session] {

  def compare(that: Session): Int = this.size compare that.size
}

sealed trait Task[T] {

  def rdd: RDD[T]

  def top: Int

  def parallelism: Int = Runtime.getRuntime.availableProcessors() + 2

  def execute(): Unit
}

case class TopChart(rdd: RDD[Chart], top: Int, topSession: Int) extends Task[Chart] {
  override def execute(): Unit =
    rdd
      .map(chart => {
        val ts = Runner.epoch(chart.timestamp)
        (chart.userid, Session(ts, ts, 1, List(chart.traname)))
      })
      .aggregateByKey(List[Session]())(Runner.aggMerge, (coll1: List[Session], coll2: List[Session]) => coll1 ::: coll2)
      .flatMap(userRow => userRow._2.foldLeft(List[Session]())(Runner.aggMerge))
      .top(topSession)
      .flatMap(_.tracks)
      .foldLeft(Map.empty[String, Int]) { (count, track) => count + (track -> (count.getOrElse(track, 0) + 1)) }
      .toSeq.sortBy(-(_: (String, Int))._2)
      .take(top)
      .foreach(println)
}