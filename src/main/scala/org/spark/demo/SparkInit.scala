package org.spark.demo

import org.apache.spark.sql.{DataFrameReader, SparkSession}

trait SparkInit {

  val spark: SparkSession = SparkSession.builder()
    .appName("Spark Demo")
    .master("local[*]")
    .getOrCreate()

  def reader: DataFrameReader = spark.read
    .option("header", false)
    .option("mode", "DROPMALFORMED")

  def close(): Unit = {
    spark.close()
  }
}