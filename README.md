# This repository contains spark demos #

### The purpose of this repository is to solve the following problem: given the dataset, produce a list of top 10 songs played in the top 50 longest user sessions by tracks count. Each user session may be comprised of one or more songs played by that user, where each song is started within 20 minutes of the previous song's start time. ###

### How do I get set up? ###

* clone repository to your computer
* to build, change to project root and execute `gradlew build shadow`
* download csv from http://www.dtic.upf.edu/~ocelma/MusicRecommendationDataset/lastfm-1K.html
* to run, change to $project-root/build/libs and execute `java -jar spark-tutorial.jar your-lastfm-file.csv`
